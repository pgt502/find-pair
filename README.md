# GIFT CARD PICKER #

## Requirements
* Go environment (1.10)

## Instructions
* Clone the repo: 
```
git clone git@bitbucket.org:pgt502/find-pair.git
```
* Go to "cmd" folder: 
```
cd cmd
```
* Build the program: 
```
go build -o find-pair
```
* Run: 
```
./find-pair path/to/file limit
``` 
where `limit` is the max price (integer) and `path/to/file` is the input file, for example:
```
./find-pair ../reader/testdata/prices.txt 2500
```

## Other options
* To run in verbose mode: 
```
./find-pair -debug=true path/to/file limit
``` 
where `limit` is the max price (integer) and `path/to/file` is the input file

* To specify the number of gift cards to be picked use `-n` parameter, e.g. to find 3 gift cards: 
```
./find-pair -n=3 /path/to/file limit
```
For example:
```
./find-pair -n=3 ../reader/testdata/prices.txt 7700
```
