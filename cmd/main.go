package main

import (
	"bytes"
	"flag"
	"fmt"
	"sort"
	"strconv"
	"time"

	"bitbucket.org/pgt502/find-pair/logger"
	"bitbucket.org/pgt502/find-pair/pick"
	"bitbucket.org/pgt502/find-pair/reader"
	"bitbucket.org/pgt502/find-pair/types"
)

var (
	debug = flag.Bool("debug", false, "whether to print debug information")
	depth = flag.Int("n", 2, "how many cards to pick - default: 2")
)

func main() {
	flag.Parse()
	logger.SetDebug(*debug)
	file := flag.Arg(0)
	limitStr := flag.Arg(1)
	var err error
	if file == "" {
		err = fmt.Errorf("file needs to be provided as first argument")
		logger.Fatal(err)
	}
	if limitStr == "" {
		err = fmt.Errorf("price limit needs to be provided as second argument")
		logger.Fatal(err)
	}
	limit, err := strconv.Atoi(limitStr)
	if err != nil {
		err = fmt.Errorf("price limit in incorrect format: %s", err)
		logger.Fatal(err)
	}
	start := time.Now()
	r := reader.NewGiftCardReader()

	cont, err := r.Read(file)
	if err != nil {
		err = fmt.Errorf("failed to read giftcards: %s", err)
		logger.Fatal(err)
	}
	logger.Printf("read file after: %s\n", time.Since(start))
	container := types.GiftCardSortedByPriceDesc(cont)

	sort.Sort(container)
	logger.Printf("sorted file after: %s\n", time.Since(start))
	var cards []*types.GiftCard
	// if the required number of items is 2 then use the picker optimised for picking just 2 items
	if *depth == 2 {
		picker := pick.NewPicker()
		cards, err = picker.Pick(container, int64(limit))
	} else {
		picker := pick.NewStackPicker()
		cards, err = picker.Pick(container, int64(limit), *depth)
	}
	if err != nil {
		logger.Println(err)
		fmt.Println(err)
		return
	}
	logger.Printf("picked solution after: %s\n", time.Since(start))
	var buf bytes.Buffer
	for i := len(cards) - 1; i >= 0; i-- {
		buf.WriteString(fmt.Sprintf("%s %d", cards[i].Product, cards[i].Price))
		if i > 0 {
			buf.WriteString(", ")
		}
	}
	fmt.Println(buf.String())
}
