package pick

import (
	"fmt"

	"bitbucket.org/pgt502/find-pair/logger"
	"bitbucket.org/pgt502/find-pair/types"
)

type Picker2 interface {
	Pick([]*types.GiftCard, int64) ([]*types.GiftCard, error)
}

type picker struct {
}

func NewPicker() Picker2 {
	return &picker{}
}

const NOT_POSSIBLE = "Not possible"

func (p *picker) Pick(input []*types.GiftCard, limit int64) (output []*types.GiftCard, err error) {
	if input == nil || len(input) == 0 || len(input) == 1 {
		err = fmt.Errorf(NOT_POSSIBLE)
		return
	}

	remaining := limit

	index := 0
	output = make([]*types.GiftCard, 0)

	for {
		logger.Printf("MAIN loop, at index %d, remaining: %d\n", index, remaining)
		if index >= len(input) {
			break
		}
		if p.pick(input, &output, limit, index, 0) {
			// found solution
			logger.Printf("found solution - exiting...len(output): %d", len(output))
			break
		} else {
			// didnt find solution - start with the next item in the array
			output = make([]*types.GiftCard, 0)
		}
		index++
	}
	if len(output) != 2 {
		output = nil
		err = fmt.Errorf(NOT_POSSIBLE)
	}
	return
}

func (p *picker) pick(input []*types.GiftCard, output *[]*types.GiftCard, limit int64, index, depth int) (ok bool) {
	logger.Printf("index: %d, limit: %d, depth: %d, output: %v", index, limit, depth, *output)
	if depth == 2 {
		logger.Println("max depth reached - exiting...")
		return limit >= 0
	}
	if index >= len(input) {
		return limit >= 0
	}
	for i := index; i < len(input); i++ {
		current := input[i]
		test := limit - current.Price
		if test >= 0 {
			limit = test
			*output = append(*output, current)
			ok = p.pick(input, output, limit, i+1, depth+1)
			return
		}
	}
	return false
}
