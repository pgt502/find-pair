package pick_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestPick(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Pick Suite")
}
