package pick_test

import (
	"sort"

	"bitbucket.org/pgt502/find-pair/logger"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "bitbucket.org/pgt502/find-pair/pick"
	"bitbucket.org/pgt502/find-pair/types"
)

var _ = Describe("Picker", func() {
	Describe("picking 2 items from the list", func() {
		var (
			input  []*types.GiftCard
			picker Picker2
		)
		BeforeEach(func() {
			logger.SetDebug(true)
			picker = NewPicker()
		})
		Context("when the input list is nil", func() {
			BeforeEach(func() {
				input = nil
			})
			It("should return an error", func() {
				actual, err := picker.Pick(input, 2000)
				Expect(err).To(MatchError(NOT_POSSIBLE))
				Expect(actual).To(BeNil())
			})
		})
		Context("when there are no items in the list", func() {
			BeforeEach(func() {
				input = make([]*types.GiftCard, 0)
				sort.Sort(types.GiftCardSortedByPriceDesc(input))
			})
			It("should return an error", func() {
				actual, err := picker.Pick(input, 2000)
				Expect(err).To(MatchError(NOT_POSSIBLE))
				Expect(actual).To(BeNil())
			})
		})
		Context("when there is only 1 item in the list", func() {
			BeforeEach(func() {
				input = []*types.GiftCard{
					{"Test", 2000},
				}
			})
			It("should return an error", func() {
				actual, err := picker.Pick(input, 2000)
				Expect(err).To(MatchError(NOT_POSSIBLE))
				Expect(actual).To(BeNil())
			})
		})
		Context("when there are 2 items in the list", func() {
			BeforeEach(func() {
				input = []*types.GiftCard{
					{"Detergent", 1500},
					{"Candy Bar", 2000},
				}
				sort.Sort(types.GiftCardSortedByPriceDesc(input))
			})
			It("should return an error when limit is too low", func() {
				actual, err := picker.Pick(input, 3000)
				Expect(err).To(MatchError(NOT_POSSIBLE))
				Expect(actual).To(BeNil())
			})
			It("should return the 2 items when their price is equal the limit", func() {
				actual, err := picker.Pick(input, 3500)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Candy Bar", 2000},
					{"Detergent", 1500},
				}
				Expect(actual).To(Equal(expected))
			})
			It("should return the 2 items when their price is lower than the limit", func() {
				actual, err := picker.Pick(input, 3600)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Candy Bar", 2000},
					{"Detergent", 1500},
				}
				Expect(actual).To(Equal(expected))
			})
		})
		Context("when there are 3 items in the list", func() {
			BeforeEach(func() {
				input = []*types.GiftCard{
					{"Headphones", 1400},
					{"Detergent", 1500},
					{"Candy Bar", 2000},
				}
				sort.Sort(types.GiftCardSortedByPriceDesc(input))
			})
			It("should return an error when limit is too low", func() {
				actual, err := picker.Pick(input, 2800)
				Expect(err).To(MatchError(NOT_POSSIBLE))
				Expect(actual).To(BeNil())
			})
			It("should return the 2 most expensive items when their price is equal the limit", func() {
				actual, err := picker.Pick(input, 3500)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Candy Bar", 2000},
					{"Detergent", 1500},
				}
				Expect(actual).To(Equal(expected))
			})
			It("should return the 2 most expensive items when their price is lower than the limit", func() {
				actual, err := picker.Pick(input, 3600)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Candy Bar", 2000},
					{"Detergent", 1500},
				}
				Expect(actual).To(Equal(expected))
			})
			It("should return the 1st and the 3rd item when this is the only combination that is lower than the limit", func() {
				actual, err := picker.Pick(input, 3400)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Candy Bar", 2000},
					{"Headphones", 1400},
				}
				Expect(actual).To(Equal(expected))
			})
			// ability to retract
			It("should return the 2 cheapest items when this is the only combination that is lower than the limit", func() {
				actual, err := picker.Pick(input, 3000)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Detergent", 1500},
					{"Headphones", 1400},
				}
				Expect(actual).To(Equal(expected))
			})
		})
		Context("when there are more items in the list", func() {
			BeforeEach(func() {
				input = []*types.GiftCard{
					{"Candy Bar", 500},
					{"Paperback Book", 700},
					{"Detergent", 1000},
					{"Headphones", 1400},
					{"Earmuffs", 2000},
					{"Bluetooth Stereo", 6000},
				}
				sort.Sort(types.GiftCardSortedByPriceDesc(input))
			})
			It("should find specified products when price is 2500", func() {
				actual, err := picker.Pick(input, 2500)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Earmuffs", 2000},
					{"Candy Bar", 500},
				}
				Expect(actual).To(Equal(expected))
			})
			It("should find specified products when price is 2300", func() {
				actual, err := picker.Pick(input, 2300)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Headphones", 1400},
					{"Paperback Book", 700},
				}
				Expect(actual).To(Equal(expected))
			})
			It("should find specified products when price is 10000", func() {
				actual, err := picker.Pick(input, 10000)
				Expect(err).To(BeNil())
				expected := []*types.GiftCard{
					{"Bluetooth Stereo", 6000},
					{"Earmuffs", 2000},
				}
				Expect(actual).To(Equal(expected))
			})
			It("should return error when price is 1100", func() {
				actual, err := picker.Pick(input, 1100)
				Expect(err).To(MatchError(NOT_POSSIBLE))
				Expect(actual).To(BeNil())
			})
		})

	})
})
