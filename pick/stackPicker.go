package pick

import (
	"fmt"

	"bitbucket.org/pgt502/find-pair/logger"

	"bitbucket.org/pgt502/find-pair/types"
)

type StackPicker interface {
	Pick(input []*types.GiftCard, limit int64, maxDepth int) (output []*types.GiftCard, err error)
}

type stackPicker struct {
}

func NewStackPicker() StackPicker {
	return &stackPicker{}
}

// Pick takes an array of Gift Cards, the limit and max depth as parameters. Max depth represents the maximum number
// of items we want returned. The algoright builds a tree from all the items iteratively utilising stacks and performas
// a Depth First Search (DFS) on the tree. It returns the 3 most expensive items that satisfy the limit or an error
// if the solution cannot be found. If wrong item is selected the algorithm can backtrack and pick the next item.
func (p *stackPicker) Pick(input []*types.GiftCard, limit int64, maxDepth int) (output []*types.GiftCard, err error) {
	if input == nil || len(input) == 0 || len(input) == 1 {
		err = fmt.Errorf(NOT_POSSIBLE)
		return
	}

	// stack for gift cards
	gStack := types.NewGiftCardStack()
	// stack for corresponding gift card positions (indexes in the input array)
	iStack := types.NewIntStack()
	remaining := limit
	l := len(input)

	// push all the items on the stack in reverse order
	for j := l - 1; j >= 0; j-- {
		in := input[j]
		logger.Printf("pushing on stack: %+v\n", in)
		gStack.Push(in)
		iStack.Push(j)
	}

	depth := 0
	found := false
	index := 0
	for {
		logger.Printf("at depth: %d, output: %+v\n", depth, output)
		if depth == maxDepth {
			found = len(output) == maxDepth
			logger.Printf("max depth - breaking out... found: %t\n", found)
			if found {
				break
			}
			// remove 1 item from output
			x := output[len(output)-1]
			// correct the remaining balance
			remaining += x.Price
			depth--
			logger.Printf("removing from output: %+v, remaining: %d\n", x, remaining)
			output = output[:len(output)-1]
		}
		if index+1 == l {
			// reached the last item in a tree node
			found = len(output) == maxDepth
			logger.Printf("last item in a tree node... found: %t\n", found)
			if found {
				break
			}
			if depth == 0 {
				logger.Println("that was the very last item - exiting...\n")
				break
			}
			// remove 1 item from output
			x := output[len(output)-1]
			// correct the remaining balance
			remaining += x.Price
			depth--
			logger.Printf("removing from output: %+v, remaining: %d\n", x, remaining)
			output = output[:len(output)-1]
		}
		p := gStack.Pop()
		index = iStack.Pop()
		if p == nil {
			logger.Println("stack empty - breaking out...")
			break
		}
		logger.Printf("popped: %+v\n", p)

		if p.Price > limit {
			logger.Printf("price too high - skipping %+v...\n", p)
			continue
		}
		test := remaining - p.Price
		if test < 0 {
			logger.Printf("price too high - skipping %+v...\n", p)
			continue
		}
		remainingDepth := maxDepth - (depth + 1)
		if test == 0 && remainingDepth > 0 {
			// if 0 money left and this is not the last item then skip it
			logger.Printf("skipping as no money left and not last item: %+v\n", p)
			continue
		}
		logger.Printf("index+1=%d, l=%d, remainingDepth=%d\n", index+1, l, remainingDepth)
		if index+1 == l && remainingDepth > 0 {
			// if this is the last item in a tree node but we have not found all items
			// we need to backtrack so we dont add this item to the output
			logger.Printf("skipping as last item in the node and not reached the goal: %+v\n")
			continue
		}
		// push on stack the children of the current item - in reverse order
		for j := l - 1; j >= index+1; j-- {
			in := input[j]
			logger.Printf("pushing on stack: %+v\n", in)
			gStack.Push(in)
			iStack.Push(j)
		}
		remaining = test
		output = append(output, p)
		depth++
	}
	if !found {
		err = fmt.Errorf(NOT_POSSIBLE)
		output = nil
	}
	return
}
