package reader_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "bitbucket.org/pgt502/find-pair/reader"
	"bitbucket.org/pgt502/find-pair/types"
)

var _ = Describe("GiftcardReader", func() {
	var (
		reader GiftCardReader
		path   string
	)
	BeforeEach(func() {
		reader = NewGiftCardReader()
	})
	Describe("reading files from disk", func() {
		Context("when file includes items", func() {
			BeforeEach(func() {
				path = "testdata/prices.txt"
			})
			It("should read the file and return the items", func() {
				cont, err := reader.Read(path)
				Expect(err).NotTo(HaveOccurred())
				expected := []*types.GiftCard{
					{"Candy Bar", 500},
					{"Paperback Book", 700},
					{"Detergent", 1000},
					{"Headphones", 1400},
					{"Earmuffs", 2000},
					{"Bluetooth Stereo", 6000},
				}
				Expect(cont).To(Equal(expected), "gift cards")
			})
		})
	})

})
