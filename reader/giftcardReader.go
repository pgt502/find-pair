package reader

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/pgt502/find-pair/logger"
	"bitbucket.org/pgt502/find-pair/types"
)

type GiftCardReader interface {
	Read(path string) ([]*types.GiftCard, error)
}

type giftCardReader struct {
}

// NewGiftCardReader returns a new instance of the GiftCardReader
func NewGiftCardReader() GiftCardReader {
	return &giftCardReader{}
}

// Read reads one line at a time and parses it to a giftcard
func (r *giftCardReader) Read(path string) (ret []*types.GiftCard, err error) {
	file, err := os.Open(path)
	if err != nil {
		logger.Printf("error reading file: %s", err)
		return
	}
	defer file.Close()
	reader := csv.NewReader(file)
	count := 1

	for {
		var row []string
		row, err = reader.Read()
		if err != nil {
			if err == io.EOF {
				err = nil
				break
			}
			logger.Printf("error reading line: %d, %s", count, err)
			return
		}
		logger.Printf("read line %d: %v\n", count, row)
		var giftCard types.GiftCard
		giftCard, err = r.parseGiftCard(row)
		if err != nil {
			logger.Printf("error parsing giftcard: %s", err)
			return
		}
		ret = append(ret, &giftCard)
		count++
	}

	return
}

func (r *giftCardReader) parseGiftCard(row []string) (card types.GiftCard, err error) {
	if l := len(row); l != 2 {
		err = fmt.Errorf("expected 2 columns, got %d", l)
		return
	}
	card.Product = strings.Trim(row[0], " ")
	price, err := strconv.Atoi(strings.Trim(row[1], " "))
	if err != nil {
		err = fmt.Errorf("failed to parse price '%s' to integer, err: %s", row[1], err)
		return
	}
	card.Price = int64(price)
	logger.Printf("parsed card: %+v\n", card)
	return
}
