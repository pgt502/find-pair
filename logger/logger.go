package logger

import (
	"log"
)

var debug = false

// SetDebug sets the debug flag which controls whether the message will be logged or not
func SetDebug(val bool) {
	debug = val
}

// Printf calls Output to print to the standard logger if debug is true
func Printf(format string, v ...interface{}) {
	if debug {
		log.Printf(format, v...)
	}
}

// Println calls Output to print to the standard logger if debug is true
func Println(v ...interface{}) {
	if debug {
		log.Println(v...)
	}
}

// Fatal calls the log.Fatal function
func Fatal(v ...interface{}) {
	log.Fatal(v...)
}

// Fatalf calls the log.Fatalf function
func Fatalf(format string, v ...interface{}) {
	log.Fatalf(format, v...)
}
