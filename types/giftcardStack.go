package types

type giftCardStack struct {
	stack []*GiftCard
}

func NewGiftCardStack() GiftCardContainer {
	h := &giftCardStack{
		stack: make([]*GiftCard, 0),
	}
	return h
}

func (s giftCardStack) Len() int {
	return len(s.stack)
}

func (s *giftCardStack) Push(card *GiftCard) {
	// push at the end
	s.stack = append(s.stack, card)
}

func (s *giftCardStack) Pop() *GiftCard {
	l := len(s.stack)
	if l == 0 {
		return nil
	}
	// remove from the end
	x := s.stack[l-1]
	s.stack = s.stack[:l-1]
	return x
}

func (s *giftCardStack) Peek() *GiftCard {
	l := len(s.stack)
	if l == 0 {
		return nil
	}
	return s.stack[l-1]

}
