package types_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "bitbucket.org/pgt502/find-pair/types"
)

var _ = Describe("GitfCardHeap", func() {
	var (
		heap GiftCardContainer
	)
	BeforeEach(func() {
		heap = NewGiftCardMaxHeap()
	})

	Context("with 0 items", func() {
		It("pop should return nil", func() {
			actual := heap.Pop()
			Expect(actual).To(BeNil())
		})
	})
	Context("with multiple items in reverse order", func() {
		BeforeEach(func() {
			heap.Push(&GiftCard{Price: 1})
			heap.Push(&GiftCard{Price: 2})
			heap.Push(&GiftCard{Price: 3})
		})
		It("pop should return the most expensive item", func() {
			actual := heap.Pop()
			Expect(actual.Price).To(BeEquivalentTo(3))
			actual = heap.Pop()
			Expect(actual.Price).To(BeEquivalentTo(2))
			actual = heap.Pop()
			Expect(actual.Price).To(BeEquivalentTo(1))
		})
	})
	Context("with multiple items in order", func() {
		BeforeEach(func() {
			heap.Push(&GiftCard{Price: 3})
			heap.Push(&GiftCard{Price: 2})
			heap.Push(&GiftCard{Price: 1})
		})
		It("pop should return the most expensive item", func() {
			actual := heap.Pop()
			Expect(actual.Price).To(BeEquivalentTo(3))
			actual = heap.Pop()
			Expect(actual.Price).To(BeEquivalentTo(2))
			actual = heap.Pop()
			Expect(actual.Price).To(BeEquivalentTo(1))
		})
	})
	Context("with multiple repeated items in mixed order", func() {
		BeforeEach(func() {
			heap.Push(&GiftCard{Price: 3})
			heap.Push(&GiftCard{Price: 1})
			heap.Push(&GiftCard{Price: 5})
			heap.Push(&GiftCard{Price: 2})
			heap.Push(&GiftCard{Price: 4})
			heap.Push(&GiftCard{Price: 1})
		})
		It("pop should return the most expensive item", func() {
			Expect(heap.Pop().Price).To(BeEquivalentTo(5))
			Expect(heap.Pop().Price).To(BeEquivalentTo(4))
			Expect(heap.Pop().Price).To(BeEquivalentTo(3))
			Expect(heap.Pop().Price).To(BeEquivalentTo(2))
			Expect(heap.Pop().Price).To(BeEquivalentTo(1))
			Expect(heap.Pop().Price).To(BeEquivalentTo(1))
		})
	})

})
