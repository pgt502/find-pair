package types

import "sort"

type GiftCardSortedByPriceDesc []*GiftCard

func (s GiftCardSortedByPriceDesc) Len() int {
	return len(s)
}

func (s GiftCardSortedByPriceDesc) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s GiftCardSortedByPriceDesc) Less(i, j int) bool {
	return s[i].Price > s[j].Price
}

func NewSortedGiftCardContainer(g []*GiftCard) GiftCardContainer {
	arr := GiftCardSortedByPriceDesc(g)
	sort.Sort(arr)
	return &arr
}

func (s *GiftCardSortedByPriceDesc) Sort() {
	sort.Sort(s)
}

func (s *GiftCardSortedByPriceDesc) Push(card *GiftCard) {
	// max at the beginning
	*s = append([]*GiftCard{card}, *s...)
}

func (s *GiftCardSortedByPriceDesc) Pop() *GiftCard {
	old := *s
	n := len(old)
	x := old[0]
	*s = old[1:n]
	return x
}

func (s *GiftCardSortedByPriceDesc) Peek() *GiftCard {
	old := *s
	x := old[0]
	return x
}
