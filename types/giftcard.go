package types

import (
	"fmt"
)

type GiftCard struct {
	Product string
	Price   int64
}

func (g *GiftCard) String() string {
	return fmt.Sprintf("{%s, %d}", g.Product, g.Price)
}

type GiftCardContainer interface {
	Push(*GiftCard)
	Pop() *GiftCard
	Len() int
}
