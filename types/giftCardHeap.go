package types

import "container/heap"

// A giftCardMaxHeap implements heap.Interface and holds Items.
type giftCardMaxHeap []*GiftCard

type GiftCardMaxHeap struct {
	giftCardMaxHeap
}

func NewGiftCardMaxHeap() GiftCardContainer {
	h := &GiftCardMaxHeap{}
	heap.Init(&h.giftCardMaxHeap)
	return h
}

func (h giftCardMaxHeap) Len() int { return len(h) }

func (h giftCardMaxHeap) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, price
	return h[i].Price > h[j].Price
}

func (h giftCardMaxHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *giftCardMaxHeap) Push(x interface{}) {
	item := x.(*GiftCard)
	*h = append(*h, item)
}

func (h *giftCardMaxHeap) Pop() interface{} {
	old := *h
	n := len(old)
	if n == 0 {
		return nil
	}
	item := old[n-1]
	*h = old[0 : n-1]
	return item
}

func (s *GiftCardMaxHeap) Push(card *GiftCard) {
	// max at the beginning
	heap.Push(&s.giftCardMaxHeap, card)
}

func (s *GiftCardMaxHeap) Pop() *GiftCard {
	if s.Len() == 0 {
		return nil
	}
	x := heap.Pop(&s.giftCardMaxHeap)
	if x == nil {
		return nil
	}
	return x.(*GiftCard)
}
