package types

type IntStack []int

func NewIntStack() *IntStack {
	arr := make([]int, 0)
	s := IntStack(arr)
	return &s
}

func (s *IntStack) Push(v int) {
	*s = append(*s, v)
}

func (s *IntStack) Pop() int {
	l := len(*s)
	x := (*s)[l-1]
	*s = (*s)[:l-1]
	return x
}
