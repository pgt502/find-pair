package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"bitbucket.org/pgt502/find-pair/logger"
)

var (
	debug = flag.Bool("debug", false, "whether to print debug information")
)

func main() {
	flag.Parse()
	logger.SetDebug(*debug)
	fmt.Printf("debug value: %t\n", *debug)
	readArgAsInt := func(index int) int64 {
		str := flag.Arg(index)
		if str == "" {
			logger.Fatalf("expected argument at index: %d", index)
		}
		val, err := strconv.ParseInt(str, 10, 64)
		if err != nil {
			logger.Fatalf("wrong argument format at index: %d, expected integer, got %s", index, str)
		}
		return val
	}
	min := readArgAsInt(0)
	max := readArgAsInt(1)
	rows := readArgAsInt(2)
	file := flag.Arg(3)
	generateData(min, max, int(rows), file)
}

func generateData(min, max int64, rows int, path string) {
	file, err := os.Create(path)
	if err != nil {
		logger.Printf("error opening file: %s", err)
		return
	}
	defer file.Close()
	rand.Seed(time.Now().UnixNano())

	for i := 0; i < rows; i++ {
		price := (rand.Int63n(max-min) + min) * 100
		_, err := file.WriteString(fmt.Sprintf("Product %d, %d\n", i+1, price))
		if err != nil {
			logger.Fatalf("error writing to file: %s", err)
		}
		if i%100 == 0 {
			file.Sync()
		}
	}
	file.Sync()
}
